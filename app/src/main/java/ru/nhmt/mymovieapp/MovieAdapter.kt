package ru.nhmt.mymovieapp

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import ru.nhmt.mymovieapp.databinding.MovieItemBinding

// Ctrl + O или Ctrl + I
class MovieAdapter(var list: List<itemMovie>, val context: Context) : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    inner class ViewHolder(val bind: MovieItemBinding) : RecyclerView.ViewHolder(bind.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val bind = MovieItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(bind)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind.title.text = list[position].title
        holder.bind.image.setImageResource(list[position].image)
        holder.bind.root.setOnClickListener {
            Toast.makeText(context, "Click on $position", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount() = list.size

}