package ru.nhmt.mymovieapp

data class itemMovie(
    val id: Int,
    val title: String,
    val image: Int
)
