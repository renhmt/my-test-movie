package ru.nhmt.mymovieapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import ru.nhmt.mymovieapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

//    val array = arrayOf("Марсианин","Аватар")

    //Подключаем разметку помощью viewBinding
    private lateinit var bind: ActivityMainBinding // В соответствии с названием activity_main + binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Инициализация bind помощью layoutinflater
        bind = ActivityMainBinding.inflate(layoutInflater)

//        val cont = LinearLayout(this)
        setContentView(bind.root) // bind.root = CoordinatorLayout

        bind.movieList.adapter = MovieAdapter(
            listOf(
                itemMovie(0, "Марс", R.drawable.ic_launcher_background),
                itemMovie(1, "Марс 2", R.drawable.ic_launcher_foreground),
            ),
            this
        )

//        cont.orientation = LinearLayout.VERTICAL
//        array.forEach {
//            val btn = Button(this)
//            btn.text = it
//            btn.setOnClickListener {
//            }
//            cont.addView(btn)
//        }

    }

}